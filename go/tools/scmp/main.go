package main

import (
	"github.com/scionproto/scion/go/tools/scmp/scmp"
	"os"
)

func main() {
	os.Exit(scmp.Main())
}

