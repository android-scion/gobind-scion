package scmp

import (
	"github.com/scionproto/scion/go/bind"
	"github.com/scionproto/scion/go/tools/scmp/scmp"
)

func Main(argsQuery, envQuery, chDir string) int {
	bind.AppendQueryEncodedArgs(argsQuery)
	bind.SetQueryEncodedEnv(envQuery)
	bind.ChangeWorkingDirectory(chDir)
	return scmp.Main()
}
