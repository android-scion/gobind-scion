package snet

import (
	"log"

	"github.com/scionproto/scion/go/bind"
	"github.com/scionproto/scion/go/lib/snet"
)

var (
	udpConnection snet.Conn
)

func Init(chdir, sciondPath, dispatcherPath, clientAddress string) {
	var (
		err   error
		local *snet.Addr
	)

	bind.ChangeWorkingDirectory(chdir)

	local, err = snet.AddrFromString(clientAddress)
	check(err)

	err = snet.Init(local.IA, sciondPath, dispatcherPath)
	check(err)
}

func DialScion(serverAddress, clientAddress string) {
	var (
		err    error
		local  *snet.Addr
		remote *snet.Addr
	)

	local, err = snet.AddrFromString(clientAddress)
	check(err)

	remote, err = snet.AddrFromString(serverAddress)
	check(err)

	udpConnection, err = snet.DialSCION("udp4", local, remote)
	check(err)
}

func Write(data []byte) {
	var (
		err error
	)

	_, err = udpConnection.Write(data)
	check(err)
}

func ReadFrom(bufferSize int) []byte {
	var (
		err error
	)

	receivePacketBuffer := make([]byte, bufferSize)

	n, _, err := udpConnection.ReadFrom(receivePacketBuffer)
	check(err)
	return receivePacketBuffer[:n]
}

func check(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
