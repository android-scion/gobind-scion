package bind

import (
	"fmt"
	"net/url"
	"os"
	"strings"
)

var (
	printer = func (format string, a... interface{}) {_, _ =fmt.Fprintf(os.Stderr, format, a...)}
)

func SetPrinter(p func (format string, a... interface{})) {
	printer = p
}

func AppendQueryEncodedArgs(query string) {
	if query != "" {
		args := strings.Split(query, "&")
		for i, arg := range args {
			if a, err := url.QueryUnescape(arg); err == nil {
				args[i] = a
			} else {
				printer("Failed to decode argument '%s': %v\n", arg, err)
			}
		}
		os.Args = append(os.Args, args...)
	}
}

func SetQueryEncodedEnv(query string) {
	if query != "" {
		envKVs := strings.Split(query, "&")
		for _, envKV := range envKVs {
			kv := strings.Split(envKV, "=")
			if len(kv) >= 2 {
				var key, value string
				if k, err := url.QueryUnescape(kv[0]); err == nil {
					key = k
				} else {
					printer("Failed to decode env variable name '%s': %v\n", kv[0], err)
					continue
				}
				if v, err := url.QueryUnescape(kv[1]); err == nil {
					value = v
				} else {
					printer("Failed to decode env variable value '%s': %v\n", kv[1], err)
					continue
				}
				if err := os.Setenv(key, value); err != nil {
					printer("Failed to set env variable '%s' to value '%s': %v\n", key, value, err)
				}
			}
		}
	}
}

func ChangeWorkingDirectory(dir string)  {
	if dir != "" {
		if err := os.Chdir(dir); err != nil {
			printer("Failed to change working directory to '%s': %v\n", dir, err)
		}
	}
}
