package sciond

import (
	"github.com/scionproto/scion/go/bind"
	"github.com/scionproto/scion/go/sciond/sciond"
)

func Main(argsQuery, envQuery, chDir string) int {
	bind.AppendQueryEncodedArgs(argsQuery)
	bind.SetQueryEncodedEnv(envQuery)
	bind.ChangeWorkingDirectory(chDir)
	return sciond.RealMain()
}
